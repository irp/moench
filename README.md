# Processing of MOENCH cluster files

Everything should be done from a python script or jupyter notebook.

## Installation
Requires a somewhat recent compiler. On Centos 7, you might have to run `scl enable devtoolset-8 -- bash`.

 ```
git clone git@gitlab.gwdg.de:irp/moench.git moench-git # pfad zur lokalen Kopie des Repositories
cd moench-git
git submodule init
git submodule update
python3 -m pip install --user .
 ```
