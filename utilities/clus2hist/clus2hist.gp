reset

set term pdf
set out 'clus2hist.pdf'

set multiplot layout 2,1
plot '/tmp/probe2_run_far10_bis_0.hist' u ($1/150):2 w l
set logscale y
plot '/tmp/probe2_run_far10_bis_0.hist' u ($1/150):2 w l
unset multiplot

set out

