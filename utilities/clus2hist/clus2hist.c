#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <stdint.h>

struct data {
	int32_t n;
	int16_t x;
	int16_t y;
	int32_t d[9];
};
const int M = 8192;

int main(int argc, const char *argv[])
{
	int ret = 0;
	const char* fname = NULL;
	int fd = -1;
	struct stat st;
	struct data* d = NULL;
	int n, N;
	int hist[M];

	if (argc != 2)
	{
		fprintf(stderr, "usage: %s <fname>\n", argv[0]);
		ret = -1;
		goto cleanup;
	}

	fname = argv[1];
	fd = open(fname, O_RDONLY);
	if (fd < 0)
	{
		fprintf(stderr, "cannot open %s: %s\n",
				fname, strerror(errno));
		ret = -1;
		goto cleanup;
	}
	if (fstat(fd, &st) != 0)
	{
		fprintf(stderr, "cannot stat %s: %s\n",
				fname, strerror(errno));
		ret = -1;
		goto cleanup;
	}

	d = mmap(NULL, st.st_size, PROT_READ, MAP_SHARED, fd, 0);
	if (d == NULL)
	{
		fprintf(stderr, "cannot mmap %s: %s\n",
				fname, strerror(errno));
		ret = -1;
		goto cleanup;
	}

	for (n=0; n<M; n++) hist[n] = 0;

	N = st.st_size / sizeof(struct data);
	fprintf(stderr, "%.3f GiB, %d events in %s ...\n",
			1.0*st.st_size/1073741824, N, fname);

	for (n=0; n<N; n++)
	{
		int s;
		int s1 = d[n].d[0]+d[n].d[1]+d[n].d[3]+d[n].d[4];
		int s2 = d[n].d[1]+d[n].d[2]+d[n].d[4]+d[n].d[5];
		int s3 = d[n].d[3]+d[n].d[4]+d[n].d[6]+d[n].d[7];
		int s4 = d[n].d[4]+d[n].d[5]+d[n].d[7]+d[n].d[8];

		if ( (n%100) == 0)
		{
			fprintf(stderr, "\r%10d %.1f%% ...", n, 100.0*n/N);
			fflush(stderr);
		}

		/* real-space ROI */
		if (d[n].x < 200) continue;

		s = s1;
		if (s2 > s) s = s2;
		if (s3 > s) s = s3;
		if (s4 > s) s = s4;
		if (s<0) continue;
		if (s>=M) s=M-1;

		hist[s]++;
	}
	fprintf(stderr, "\r%10d 100%%     \n", n);

	for (n=0; n<M; n++)
		fprintf(stdout, "%6.3f %d\n", 1.0*n/150, hist[n]);

cleanup:
	if (d)    { munmap(d, st.st_size); d  = NULL; }
	if (fd>0) { close(fd);             fd =   -1; }
	return ret;
}

