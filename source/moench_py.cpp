#include <pybind11/eigen.h>
#include <pybind11/iostream.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "./moench.h"

namespace py = pybind11;

PYBIND11_MODULE(_moench, m) {
  m.doc() = "MOENCH detector io";

  py::class_<moench::ClusterFileProcessor>(m, "ClusterFileProcessor")
      .def(py::init<const moench::Gainmap &>())
      .def("process_file_raw", &moench::ClusterFileProcessor::process_file_raw,
           py::call_guard<py::scoped_ostream_redirect>(),
           py::return_value_policy::move);

  m.attr("nx") = py::int_(moench::nx);
  m.attr("ny") = py::int_(moench::ny);
}
