#pragma once

#include <Eigen/Dense>
#include <algorithm>
#include <array>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <string>
#include <utility>
#include <vector>

namespace moench {

const int nx = 400;  // number of columns
const int ny = 400;  // number of rows

using EnergyScalar = float;
using CountScalar = int64_t;
using CoordinateScalar = double;  // float causes problems when wrapping eta

template <typename T>
using ArrayXX = Eigen::Array<T, Eigen::Dynamic, Eigen::Dynamic>;
template <typename T>
using ArrayX = Eigen::Array<T, Eigen::Dynamic, 1>;
template <typename T>
using Array33 = Eigen::Array<T, 3, 3>;
template <typename T>
using Array22 = Eigen::Array<T, 2, 2>;

using Gainmap = Eigen::Array<EnergyScalar, Eigen::Dynamic, Eigen::Dynamic>;
using Coordinate = Eigen::Array<CoordinateScalar, 2, 1>;

using PixelIndex = Eigen::Array<int16_t, 2, 1>;
using Index2 = Eigen::Array<Eigen::Index, 2, 1>;

struct frame_header {
  int iframe;  // frame number
  int nph;     // number of photons in frame
};

template <int dx = 3, int dy = 3>
struct single_photon_hit {
  static const int kLength = dx * dy;
  int16_t x;  // x-coordinate of the center of hit
  int16_t y;  // x-coordinate of the center of hit
  int data[kLength];
};

const size_t  event_chunksize = 1000000;
constexpr size_t photonbuf_size = 2 * event_chunksize;

template <class T>
int argmax(T container) {
  auto max_iterator =
      std::max_element(std::begin(container), std::end(container));
  return std::distance(std::begin(container), max_iterator);
}

template <typename T>
class indexmap {
 public:
  const T min, max;
  const int num_bins;
  const T reciprocal_width;

  indexmap(T min, T max, Eigen::Index num_bins)
      : min(min),
        max(max),
        num_bins(num_bins),
        reciprocal_width(static_cast<T>(num_bins) / (max - min)) {}

  Eigen::Index operator()(T x) const {
    return std::floor((x - min) * reciprocal_width);
  }
};

class indexmap2d {
 public:
  const indexmap<CoordinateScalar> indexmap1;
  const indexmap<CoordinateScalar> indexmap2;

  indexmap2d(Coordinate min, Coordinate max, Index2 num_bins)
      : indexmap1(min(0), max(0), num_bins(0)),
        indexmap2(min(1), max(1), num_bins(1)) {}

  Index2 operator()(Coordinate x) const {
    return {indexmap1(x(0)), indexmap2(x(1))};
  }

  Eigen::Index get1d(Coordinate x, Index2 stride) const {
    Index2 idx = operator()(std::move(x));

    return idx.matrix().dot(stride.matrix());
  }
};

/* Helper data structure to analyze single photon events */
struct photon_event {
  int iframe = 0;
  PixelIndex position = PixelIndex::Zero();
  Array22<EnergyScalar> quad = Array22<EnergyScalar>::Zero();
  EnergyScalar energy = 0;
};

class PreProcessor {
 public:
  const Gainmap gainmap;

  explicit PreProcessor(const Gainmap &gainmap) : gainmap{gainmap} {}

  photon_event process(const single_photon_hit<3, 3> &cluster,
                       const int iframe) const {
    photon_event event;

    // drop clusters on the frame boundary
    if (!(cluster.x > 0 && cluster.x < nx - 1 && cluster.y > 0 &&
          cluster.y < ny - 1))
      return event;

    const Eigen::Map<Array33<int>> raw_data(
        const_cast<int *>(&(cluster.data[0])), 3, 3);
    const PixelIndex position = {cluster.x - 1, cluster.y - 1};
    const Array33<EnergyScalar> data =
        raw_data.cast<EnergyScalar>() *
        gainmap.block<3, 3>(position[1], position[0]);

    // TODO(Leon): static machen
    // offset of the 2x2-quadrant relative to the center pixel of the
    // 3x3-cluster (x, y)
    const std::array<PixelIndex, 4> quadrant_offsets = {
        {PixelIndex(0, 0), PixelIndex(0, 1), PixelIndex(1, 0),
         PixelIndex(1, 1)}};

    const std::array<EnergyScalar, 4> quadrant_sums = {
        {data.block<2, 2>(0, 0).sum(),    // top left
         data.block<2, 2>(0, 1).sum(),    // bottom left
         data.block<2, 2>(1, 0).sum(),    // top right
         data.block<2, 2>(1, 1).sum()}};  // bottom right

    // find maximal quadrant
    int maximal_quadrant = argmax(quadrant_sums);

    EnergyScalar quad_sum = quadrant_sums[maximal_quadrant];
    EnergyScalar tot_sum = data.sum();

    // drop pileups and weird events
    // TODO(Leon): magic numbers!!
    if (!(quad_sum > 0.8 * tot_sum && quad_sum < 1.2 * tot_sum)) {
      return event;
    }

    PixelIndex offset = quadrant_offsets[maximal_quadrant];
    event.position = position + offset;
    event.quad = data.block<2, 2>(offset[0], offset[1]);
    event.energy = tot_sum;
    event.iframe = iframe;

    return event;
  }
};

class EventProcessor {
 public:
  const EnergyScalar min_energy;
  const EnergyScalar max_energy;
  const int num_bins;
  const indexmap<EnergyScalar> energy_indexmap;
  const int n_int;
  const indexmap2d image_indexmap;
  const Eigen::Index image_outer_stride;
  const Index2 image_stride;

  EventProcessor(const EnergyScalar min_energy, const EnergyScalar max_energy,
                 const int num_bins)
      : min_energy{min_energy},
        max_energy{max_energy},
        num_bins{num_bins},
        energy_indexmap{min_energy, max_energy, num_bins},
        n_int{1},
        image_indexmap{Coordinate(0, 0), Coordinate(nx, ny),
                       Index2(nx * n_int, ny * n_int)},
        image_outer_stride{nx * n_int * ny * n_int},
        image_stride{1, n_int * nx} {}

  Eigen::Index max() const { return image_outer_stride * num_bins; }

  inline Eigen::Index operator() (const photon_event &event) const {
    // drop events out of energy range
    if (!(event.energy >= min_energy && event.energy < max_energy)) {
      return -1;
    }

    // TODO(Leon): mask bad pixels
    const Eigen::Index energy_index = energy_indexmap(event.energy);

    // calculate eta
    Coordinate eta = Coordinate::Zero();
    EnergyScalar quadsum = event.quad.sum();
    eta(0) = (event.quad(1, 0) + event.quad(1, 1)) / quadsum;  // x
    eta(1) = (event.quad(0, 1) + event.quad(1, 1)) / quadsum;  // y

    // eta can exceed the interval [0,1) due to negative charge-values
    // here, we wrap eta back into the unit interval and add the integer
    // offset to the pixel coordinate
    Coordinate eta_offset = eta.floor();
    eta = eta - eta_offset;

    Coordinate quad_position = event.position.cast<CoordinateScalar>();

    // TODO(Leon): hier wird interpoliert via pos_fine(eta, energy)
    Coordinate interpolated_position =
        quad_position + 0.5;  // + eta_offset + pos_fine;

    const Eigen::Index image_pixel_index =
        image_indexmap.get1d(interpolated_position, image_stride);

    return image_outer_stride * energy_index + image_pixel_index;
  }
};

struct Statistics {
  CountScalar nph_used = 0;   // number of used clusters
  CountScalar nph_total = 0;  // total number of processed clusters
  CountScalar nframes = 0;    // total number of processed frames
  ArrayX<CountScalar> nph_frame_hist =
      ArrayX<CountScalar>::Zero(nx * ny + 1);  // histogram of photons per frame
};

class ClusterFileProcessor {
 public:
  const Gainmap gainmap;
  Statistics statistics;

  explicit ClusterFileProcessor(const Gainmap &gainmap)
      : gainmap{gainmap}, statistics() {}

  ArrayX<CountScalar> process_file_raw(const std::string &input_filename,
                                       const EnergyScalar min_energy,
                                       const EnergyScalar max_energy,
                                       const int num_bins) {
    const PreProcessor preprocessor{gainmap};
    const EventProcessor processor{min_energy, max_energy, num_bins};

    ArrayX<CountScalar> images(processor.max());
    images = 0;

    std::ifstream clustfile;
    clustfile.open(input_filename, std::ios_base::in | std::ios_base::binary);

    if (!clustfile.is_open()) {
      throw std::runtime_error("could not open file " + input_filename);
    }

    std::vector<photon_event> eventbuf(
        photonbuf_size);  // preprocessed photon events
    std::vector<Eigen::Index> image_indexbuf(
        photonbuf_size);  // indices for hitmap

    while (!clustfile.eof()) {
      size_t photons_read = 0;

      // read and process in chunks, to process large files without using too much memory
      while (photons_read < event_chunksize) {
        // read frame header
        frame_header header;
        if (!clustfile.read(reinterpret_cast<char *>(&header), sizeof(frame_header))) {
          if (clustfile.eof()) {
            break;
          }
          if (clustfile.bad()) {
            throw std::runtime_error("Error while reading frame header");
          }
        }

        // check if header is valid
        if (header.nph < 0) {
          throw std::runtime_error(
              "Invalid frame header with negative event count.");
        }
        const size_t nph = static_cast<size_t>(header.nph);
        const int iframe = header.iframe;

        if (photons_read + nph >= photonbuf_size) {
          throw std::runtime_error("Eventbuf overflow. Increase photonbuf_size");
        }

        // read frame data
        for (size_t j = 0; j < nph; ++j) {
          single_photon_hit<3, 3> cl;
          clustfile.read(reinterpret_cast<char *>(&cl), sizeof(single_photon_hit<3, 3>));
          eventbuf[photons_read + j] = preprocessor.process(cl, iframe);
        }

        if (!clustfile.good()) {
          throw std::runtime_error("Could not finish reading frame");
        }

        statistics.nframes += 1;
        statistics.nph_total += nph;
        photons_read += nph;
      }

      std::transform(eventbuf.cbegin(), eventbuf.cend(), image_indexbuf.begin(),
         processor);

      // sort for better cache coherence
      std::sort(image_indexbuf.begin(), image_indexbuf.begin() + photons_read);
      auto image_begin = std::lower_bound(
          image_indexbuf.begin(), image_indexbuf.begin() + photons_read, 0);
      auto image_end = std::upper_bound(image_indexbuf.begin(),
                                        image_indexbuf.begin() + photons_read,
                                        images.size());
      std::for_each(image_begin, image_end,
                    [&images](auto const ind) { ++images(ind); });

      statistics.nph_used += std::distance(image_begin, image_end);
    }

    return images;
  }  // end method
};   // end class

}  // namespace moench
