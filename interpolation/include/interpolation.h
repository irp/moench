#ifndef INTERPOLATION_H
#define INTERPOLATION_H

#include <vector>
#include <cassert>

#include <Eigen/Dense>

using CoordinateScalar = float;
using DataScalar = float;

using Coordinate = Eigen::Array<CoordinateScalar, 2, 1>;


template <typename T>
using ImageData = Eigen::Array<T, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;

template <typename T>
struct BaseImage {
  public:

    BaseImage(int nx, int ny)
      : pixels(ny, nx) {
        pixels = 0;
      };

    ImageData<T> & get() {
      return pixels;
    }

    const ImageData<T> & get_const() const {
      return pixels;
    }

    T& operator()(int ix, int iy) {
      assert(ix >= 0);
      assert(ix < pixels.cols());
      assert(iy >= 0);
      assert(iy < pixels.rows());

      return pixels(iy, ix);;
    }

  private:
    ImageData<T> pixels;
};

template <typename T>
struct InterpolatedImage : BaseImage<T> {

  public:

    InterpolatedImage(int nx, int ny, int ns)
      : BaseImage<T>(nx * ns, ny * ns),
        n_subpixels(ns)
     { };

    T& operator[](std::array<CoordinateScalar, 2> position) {
      int ix= static_cast<CoordinateScalar>(n_subpixels) * position[0];
      int iy= static_cast<CoordinateScalar>(n_subpixels) * position[1];
      return (*this)(ix, iy);
    }

  protected:
    int n_subpixels; 
};

template <int emin=0, int emax=1, typename T=int>
struct Field : BaseImage<T> {

  static constexpr int nbeta = nb;
  static constexpr CoordinateScalar etamin = emin;
  static constexpr CoordinateScalar etamax = emax;
  static constexpr CoordinateScalar etastep = (etamax-etamin)/nbeta;

  Field(int nb)
	  : BaseImage<T>(nb, nb) {}

  inline static std::array<int, 2> etaToIndex(CoordinateScalar etax, CoordinateScalar etay){
	  assert(etax >= etamin);
	  assert(etax < etamax);
	  assert(etay >= etamin);
	  assert(etay < etamax);
    int ix = (etax-etamin)/etastep;
    int iy = (etay-etamin)/etastep;
    return { ix, iy };
  }

  T & operator[](std::array<CoordinateScalar, 2> eta) {
    auto index = etaToIndex(eta[0], eta[1]);
    return (*this)(index[0], index[1]); // (x,y)
  }
};

struct Chart2d {
	Field<0, 1, CoordinateScalar> e_x;
	Field<0, 1, CoordinateScalar> e_y;

	Chart2d(Field<0, 1, CoordinateScalar> _e_x, Field<0, 1, CoordinateScalar> _e_y)
	{
		e_x = _e_x;
		e_y = _e_y;
	}

	std::array<CoordinateScalar,2> operator()(std::array<CoordinateScalar, 2> chi) {
		std::array<CoordinateScalar,2> x;
		x[0] = e_x[chi];
		x[1] = e_y[chi];

		return x;
	}
};

#endif
