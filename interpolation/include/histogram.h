#include <iostream>
#include <exception>

#include <eigen3/Eigen/Dense>

template <typename X=double, typename Y=uint32_t>
struct Histogram1d {

  using DataType = Eigen::Array<Y, Eigen::Dynamic, 1>;

  DataType data;
  const X xmin, xmax;

  Histogram1d(size_t n_bins, X min_v, X max_v )
    : data (n_bins), xmin (min_v), xmax(max_v) {
	data = 0; }

  size_t index(X x) const {
    return std::floor((x - xmin)/(xmax - xmin) * static_cast<X>(data.size()));
  }

  Y & at(X x) {
    if (x < xmin || x >= xmax) {
      throw std::out_of_range("element " + std::to_string(x) + " is out of range");
    }

    size_t x_i = index(x);
    return data(x_i);
  }

  X get_step() const {
    return (xmax - xmin) / data.size();
  }

  void format(std::ostream & os) const {
    const char column_sep = ' ';
    const char data_sep = '\n';

    for (auto i = 0; i< data.size(); i++) {
      os << i << column_sep << data[i] << data_sep;
    }
  }
};

