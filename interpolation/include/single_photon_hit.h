#ifndef SINGLE_PHOTON_HIT_H
#define SINGLE_PHOTON_HIT_H

#include <array>
#include <iterator>
#include <algorithm>
#include <Eigen/Dense>

using DataScalar = float;
using ArrayXX = Eigen::Array<DataScalar, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;
using ArrayX = Eigen::Array<DataScalar, Eigen::Dynamic, 1>;


using Array33 = Eigen::Array<DataScalar, 3, 3>;
using Array22 = Eigen::Array<DataScalar, 2, 2>;

using RawArray33 = Eigen::Array<int, 3, 3, Eigen::RowMajor>;

using gainmap = Eigen::Array<DataScalar, Eigen::Dynamic, Eigen::Dynamic>;

using CoordinateScalar = double;
using Coordinate = Eigen::Array<CoordinateScalar, 2, 1>;

template <int dim>
using Index = Eigen::Array<int, dim, 1>;

template <int dx=3, int dy=3>
struct single_photon_hit {

  /** @short Structure for a single photon hit */

  public: 
    /** constructor, instantiates the data array  -- all class elements are public!
     */
    single_photon_hit() = default;

    static int constexpr get_length() {return sizeof(int) + 2 * sizeof(int16_t) + dx*dy*sizeof(int);};

    int iframe;         /**< frame number */
    int16_t x; 	        /**< x-coordinate of the center of hit */
    int16_t y;          /**< x-coordinate of the center of hit */
    int data[dx*dy];
};

// ensure that binary size matches 
static_assert(sizeof(single_photon_hit<3,3>) == single_photon_hit<3,3>::get_length(), "cluster size does not match");


template <class T>
int argmax(T container) {
  auto max_iterator = std::max_element(std::begin(container), std::end(container));
  return std::distance(std::begin(container), max_iterator);
}


/* Helper data structure to analyze single photon events */
struct photon_event {

  DataScalar sum;
  DataScalar totquad;

  Coordinate eta;
  Index<2> position;

  photon_event(const single_photon_hit<3,3> &hit, const Eigen::Ref<const gainmap> & gainmap)  {
    const Eigen::Map<RawArray33> raw_data (const_cast<int *>(&(hit.data[0])), 3, 3);

    position = { hit.x - 1, hit.y - 1 };

    Array33 data = raw_data.cast<DataScalar>() * gainmap.block<3,3>(position[1], position[0]);

    sum = data.sum();

    // TODO: static machen
    // offset of the 2x2-quadrant relative to the center pixel of the 3x3-cluster (x, y)
    const std::array<Index<2>,4> quadrant_offsets = {{ Index<2>(0, 0), Index<2>(0, 1), Index<2>(1, 0), Index<2>(1, 1) }};

    std::array<DataScalar, 4> quadrant_sums;
    // index in matrix convention (i, j) = (y,x)
    quadrant_sums[0] = data.block<2,2>(0, 0).sum(); // top left
    quadrant_sums[1] = data.block<2,2>(1, 0).sum(); // bottom left
    quadrant_sums[2] = data.block<2,2>(0, 1).sum(); // top right
    quadrant_sums[3] = data.block<2,2>(1, 1).sum(); // bottom right

    // find maximal quadrant
    int maximal_quadrant = argmax(quadrant_sums);

    totquad = quadrant_sums[maximal_quadrant];
    Index<2> offset = quadrant_offsets[maximal_quadrant];

    const Array22 sDum = data.block<2,2>(offset[1], offset[0]);

    position += offset;

    // calculate eta
    eta(0)=(sDum(0,1) + sDum(1,1))/totquad; // x
    eta(1)=(sDum(1,0) + sDum(1,1))/totquad; // y
  }
};

#endif
