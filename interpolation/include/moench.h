#ifndef MOENCH_H
#define MOENCH_H

#include "single_photon_hit.h"
#include <array>
#include <iostream>
#include <fstream>

#include <iomanip>  


namespace moench {

constexpr size_t buffersize = 1024 * 1024 * 10;
constexpr size_t clustersize = sizeof(single_photon_hit<3,3>);
constexpr size_t buffer_bytes = clustersize * buffersize;

const int dx = 400; // number of columns
const int dy = 400; // number of rows

template <typename T>
class indexmap {
  public:
    const T min, max;
    const int num_bins;
    const T reciprocal_width;
    
    indexmap(T min, T max, int num_bins)
      : min (min),
        max(max),
        num_bins (num_bins),
        reciprocal_width (static_cast<T>(num_bins) / (max - min))
    { };
    
    int operator() (T x) const {      
      return std::floor((x - min) * reciprocal_width);
    };
};

class indexmap2d {
  public:
    const indexmap<CoordinateScalar> indexmap1;
    const indexmap<CoordinateScalar> indexmap2;
        
    indexmap2d(Coordinate min, Coordinate max, Index<2> num_bins) 
      : indexmap1(min(0), max(0), num_bins(0)),
        indexmap2(min(1), max(1), num_bins(1)) 
    {};
    
    Index<2> operator()(Coordinate x) const {
      return {indexmap1(x(0)), indexmap2(x(1)) };
    };
    
    int get1d(Coordinate x, Index<2> stride) const {
      Index<2> idx = operator()(x);
      
      return idx.matrix().dot(stride.matrix());
    }
};

ArrayXX compute_hitmaps(const std::string & input_filename, 
                        const int hitmap_resolution, 
                        const float min_energy, 
                        const float max_energy, 
                        const int num_bins, 
                        const Eigen::Ref<const ArrayX> & gainmap_raw) {

  int nph=0;   // number of used clusters
	int totph=0; // total number processed clusters
  
  const Eigen::Map<const ArrayXX> gainmap_raw2d (gainmap_raw.data(), dy, dx); // Row Major (wegen numpy)
  const gainmap gainmap = gainmap_raw2d; // Column Major - literally 1000x faster here - !!
  
  ArrayXX hitmaps(num_bins, hitmap_resolution * hitmap_resolution);
  hitmaps = 0;
  
  const Index<2> hitmap_stride (1, hitmap_resolution);
  const indexmap<float> energy_indexmap ( min_energy, max_energy, num_bins );
  const indexmap2d eta_indexmap ( Coordinate(0,0), Coordinate(1,1), Index<2>(hitmap_resolution,hitmap_resolution) );

	std::ifstream clustfile;
	clustfile.open(input_filename, std::ifstream::binary);

	if (! clustfile.is_open()) {
		throw std::runtime_error( "could not open file " + input_filename );
	}

	// file size
	clustfile.seekg(0, std::ifstream::end);
	size_t filesize = clustfile.tellg();
	clustfile.seekg(0);

	if ( 0 < filesize % clustersize ) {
		std::cout << "WARNING: Filesize " << filesize << " not multiple of cluster size " << clustersize << std::endl;
	}

	const int num_chunks = filesize / buffer_bytes + 1;
	//int processed_chunks = 0;

  
  int chunk_size;
  size_t numclusters;
	std::vector<single_photon_hit<3,3>> inbuf(buffersize);

#pragma omp parallel
	{

		ArrayXX hitmaps_private(num_bins, hitmap_resolution * hitmap_resolution);
    hitmaps_private = 0;
    
    int totph_local = 0;
    int nph_local = 0;

		for (int chunk = 0; chunk < num_chunks; chunk++) {
      
#pragma omp single
			{
				clustfile.read(reinterpret_cast<char*>(inbuf.data()), buffer_bytes);
        chunk_size = clustfile.gcount();
        
        numclusters = chunk_size / clustersize;
        std::cout << "processing chunk " << (chunk + 1) << "/" << num_chunks << std::endl;  
			}

#pragma omp for
			for (size_t i=0; i<numclusters; i++) {
				const single_photon_hit<3,3>& cl = inbuf[i];

				totph_local++;

				// drop clusters on the frame boundary
				if (! (cl.x > 0 && cl.x < dx-1 && cl.y > 0 && cl.y < dy-1))
					continue;

				photon_event event (cl, gainmap);

				// drop double events
        // TODO: magic numbers!!
				if (! (event.totquad/event.sum>0.8 && event.totquad/event.sum<1.2) )
					continue;

				// drop events out of energy range
				if (! (event.totquad >= min_energy && event.totquad < max_energy) )
					continue;

				// TODO: mask bad pixels

				Coordinate eta = event.eta;
				
        // eta can exceed the interval [0,1) due to negative charge-values
				// here, we wrap eta back into the unit interval and add the integer
			  // offset to the pixel coordinate 
				eta = eta - eta.floor();

        float photon_energy (event.totquad);
        int energy_index = energy_indexmap(photon_energy);
        int pixel_index = eta_indexmap.get1d(eta, hitmap_stride);

        nph_local++;

        hitmaps_private(energy_index, pixel_index)++;
			} // end cluster for loop

		} // end chunk for loop


#pragma omp critical
		{
			hitmaps += hitmaps_private;
      totph += totph_local;
      nph += nph_local;
		}
	} // end parallel
  
  clustfile.close();

	std::cout << "Filled " << nph << "/"<< totph <<" (" << (static_cast<double>(nph) / static_cast<double>(totph) * 100.) << "%)" << std::endl; 
	return hitmaps;
}


ArrayXX compute_image(const std::string & input_filename, 
                      const int hitmap_resolution, 
                      const float min_energy, 
                      const float max_energy, 
                      const int num_bins, 
                      const Eigen::Ref<const ArrayX> & gainmap_raw, 
                      const int n_int,
                      const Eigen::Ref<const ArrayXX> & psi_x, 
                      const Eigen::Ref<const ArrayXX> & psi_y) {
                        
  // consistency checks
  if (! (psi_x.rows() == num_bins)) {
    throw std::runtime_error( "inconsistent number of energy bins");
  }
  
  if (! (psi_x.cols() == hitmap_resolution * hitmap_resolution)) {
    throw std::runtime_error( "inconsistent resolution of psi_x");
  }
  
  if (! ((psi_x.rows() == psi_y.rows()) || (psi_x.cols() == psi_y.cols())) ) {
    throw std::runtime_error( "inconsistent shape of psi_x and psi_y" );
  }
   
  // TODO: decouple psi energy bins from image energy bins
                        
  int nph=0;   // number of used clusters
	int totph=0; // total number processed clusters
  
  bool do_interpolate = true;
  
  const Eigen::Map<const ArrayXX> gainmap_raw2d (gainmap_raw.data(), dy, dx); // Row Major (wegen numpy)
  const gainmap gainmap = gainmap_raw2d; // Column Major - literally 1000x faster here - !!
  
  ArrayXX images(num_bins, dx * n_int * dy * n_int);
  images = 0;
  
  const Index<2> psi_stride (1, hitmap_resolution);
  const Index<2> image_stride (1, n_int * dx);
  
  const indexmap<float> energy_indexmap ( min_energy, max_energy, num_bins );
  const indexmap2d psi_indexmap ( Coordinate(0,0), Coordinate(1,1), Index<2>(hitmap_resolution, hitmap_resolution) );
  const indexmap2d image_indexmap ( Coordinate(0,0), Coordinate(dx, dy), Index<2>(dx * n_int, dy * n_int) );

	std::ifstream clustfile;
	clustfile.open(input_filename, std::ifstream::binary);

	if (! clustfile.is_open()) {
		throw std::runtime_error( "could not open file " + input_filename );
	}

	// file size
	clustfile.seekg(0, std::ifstream::end);
	size_t filesize = clustfile.tellg();
	clustfile.seekg(0);

	if ( 0 < filesize % clustersize ) {
		std::cout << "WARNING: Filesize " << filesize << " not multiple of cluster size " << clustersize << std::endl;
	}

	const int num_chunks = filesize / buffer_bytes + 1;
  
  int chunk_size;
  size_t numclusters;
	std::vector<single_photon_hit<3,3>> inbuf(buffersize);
  #pragma omp parallel
	{
    
    ArrayXX images_private(num_bins, dx * n_int * dy * n_int);
    images_private = 0;
    
    int totph_local = 0;
    int nph_local = 0;

		for (int chunk = 0; chunk < num_chunks; chunk++) {
      
#pragma omp single
			{
				clustfile.read(reinterpret_cast<char*>(inbuf.data()), buffer_bytes);
        chunk_size = clustfile.gcount();
        
        numclusters = chunk_size / clustersize;
        std::cout << "processing chunk " << (chunk + 1) << "/" << num_chunks << std::endl;  
			}

      #pragma omp for
			for (size_t i=0; i<numclusters; i++) {
				const single_photon_hit<3,3>& cl = inbuf[i];

				totph_local++;

				/// drop clusters on the frame boundary
				if (! (cl.x > 0 && cl.x < dx-1 && cl.y > 0 && cl.y < dy-1))
					continue;

				photon_event event (cl, gainmap);

				// drop double events
        // TODO: magic numbers!!
				if (! (event.totquad/event.sum>0.8 && event.totquad/event.sum<1.2) )
					continue;

				// drop events out of energy range
				if (! (event.totquad >= min_energy && event.totquad < max_energy) )
					continue;

				// TODO: mask bad pixels

				Coordinate eta = event.eta;
				
        // eta can exceed the interval [0,1) due to negative charge-values
				// here, we wrap eta back into the unit interval and add the integer
			  // offset to the pixel coordinate 
				auto eta_offset = eta.floor();
        eta = eta - eta_offset;
				
				nph_local++;
        
        float photon_energy (event.totquad);
        int energy_index = energy_indexmap(photon_energy);
        
        int pixel_index;
        
        if (do_interpolate) {
          Coordinate quad_position = event.position.cast<CoordinateScalar>();
                    
          int psi_idx = psi_indexmap.get1d(eta, psi_stride);
          
          Coordinate pos_fine;
          pos_fine(0) = psi_x(energy_index, psi_idx);
          pos_fine(1) = psi_y(energy_index, psi_idx);
          
          Coordinate interpolated_position = quad_position + eta_offset + pos_fine;
          
          pixel_index = image_indexmap.get1d(interpolated_position, image_stride);
        } else {
          Index<2> image_idx (cl.x, cl.y);
          pixel_index = image_idx.matrix().dot(image_stride.matrix());
        }

        images_private(energy_index, pixel_index)++;

			} // end cluster for loop

		} // end chunk for loop


#pragma omp critical
		{
			images += images_private;
      totph += totph_local;
      nph += nph_local;
		}
	} // end parallel


	std::cout << "Filled " << nph << "/"<< totph <<" (" << (static_cast<double>(nph) / static_cast<double>(totph) * 100.) << "%)" << std::endl; 
  
  return images;
}


}
#endif // MOENCH_H
