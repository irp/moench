#include <iostream>
#include <fstream>

#include "cxxopts.hpp"

#include "single_photon_hit.h"
#include "interpolation.h"

using namespace std;
const int NC = 400; // number of columns
const int NR = 400; // number of rows

constexpr float default_gain = 1. / 140.;

constexpr size_t buffersize = 1024 * 1024;
constexpr size_t clustersize = sizeof(single_photon_hit<3,3>);
constexpr size_t buffer_bytes = clustersize * buffersize;

std::array<int,2> wrap(std::array<double,2> & input) {
	std::array<int,2> offset;

	offset[0] = std::floor(input[0]);
	offset[1] = std::floor(input[1]);

	input[0] -= offset[0];
	input[1] -= offset[1];

	return offset;
}


int main(int argc, char *argv[]) {

#ifndef FF
	cxxopts::Options options (argv[0], " - interpolate MÖNCH03 image");
//	options.show_positional_help();

	options.add_options()
		("s,subpixels", "Number of subpixels for interpolation", cxxopts::value<int>()->default_value("1"))
		("g,gainmap", "Path to the gainmap", cxxopts::value<std::string>())
		("x,etamap_x", "Path to the etamap x", cxxopts::value<std::string>())
		("y,etamap_y", "Path to the etamap y", cxxopts::value<std::string>())
		("o,output", "Path to the output file", cxxopts::value<std::string>())
		("m,cmin", "Minimum energy in keV", cxxopts::value<float>()->default_value("0"))
		("M,cmax", "Maximum energy in keV", cxxopts::value<float>()->default_value("100"))
		("h,help", "Print help")
		("i,input", "Input file", cxxopts::value<std::vector<std::string>>());

	options.positional_help("<output> <input1> [<input2> ... ]");
	options.parse_positional({"output", "input"});
#else
	cxxopts::Options options (argv[0], " - generate eta map");

	options.add_options()
		("g,gainmap", "Path to the gainmap", cxxopts::value<std::string>())
		("e,etamap", "Path to the etamap", cxxopts::value<std::string>())
		("m,cmin", "Minimum energy in keV", cxxopts::value<float>()->default_value("0"))
		("M,cmax", "Maximum energy in keV", cxxopts::value<float>()->default_value("100"))
		("i,input", "Input file", cxxopts::value<std::vector<std::string>>());

	options.parse_positional({"etamap", "input"});
	options.positional_help("<etamap> <input1> [<input2> ... ]");
#endif


	std::vector<std::string> input_file_list;
	std::string input_filename;
	std::string badmap_filename;
	std::string gainmap_filename;

	float cmin; // lower energy threshold in keV
	float cmax; // upper energy threshold in keV

#ifndef FF
	std::string output_filename;
	std::string etamap_x_filename;
	std::string etamap_y_filename;
	int nsubpix;
	bool do_interpolate = true;
#else
	std::string etamap_filename;
#endif


	try {
		auto result = options.parse(argc, argv);

		if (result.count("help")) {
			std::cout << options.help() << std::endl;
			return EXIT_SUCCESS;
		}


		if(result.count("gainmap")) {
			gainmap_filename = result["gainmap"].as<std::string>();
		}
		std::cout << "gainmap: " << gainmap_filename << std::endl;

		cmin = result["cmin"].as<float>();
		std::cout << "cmin: " << cmin << std::endl;

		cmax = result["cmax"].as<float>();
		std::cout << "cmax: " << cmax << std::endl;

		if(result.count("input")) {
			auto &v = result["input"].as<std::vector<std::string>>();
			input_file_list = v;
		}

#ifndef FF
		if(result.count("output")) {
			output_filename = result["output"].as<std::string>();
		}else{
			std::cerr << "No output given. Aborting" << std::endl;
			return EXIT_FAILURE;
		}
		std::cout << "output file: " << output_filename << std::endl;

		nsubpix = result["subpixels"].as<int>();
		if (nsubpix < 1) {
			nsubpix = 1;
			do_interpolate = false;
			std::cout << "Interpolation disabled" << std::endl;
		}
		std::cout << "subpixels: " << nsubpix << std::endl;

		if (result.count("etamap_x")) {
			etamap_x_filename = result["etamap_x"].as<std::string>();
		}
		std::cout << "etamap_x: " << etamap_x_filename << std::endl;

		if (result.count("etamap_y")) {
			etamap_y_filename = result["etamap_y"].as<std::string>();
		}
		std::cout << "etamap_y: " << etamap_y_filename << std::endl;
#else
		if (result.count("etamap")) {
			etamap_filename = result["etamap"].as<std::string>();
		}
		std::cout << "etamap: " << etamap_filename << std::endl;
#endif


	} catch (const cxxopts::OptionException& e) {
		std::cerr << "error parsing options: " << e.what() << std::endl;
		return EXIT_FAILURE;
	}

	cout << "Buffer size: " << buffer_bytes / 1024 / 1024 << "Mb" << std::endl;

	if (input_file_list.size() < 1) {
		std::cerr << "No input files given." << std::endl;
		return EXIT_FAILURE;
	}

	input_filename = input_file_list[0];

	std::cout << "reading data from " << input_filename << std::endl;

	int nph=0;   // number of used clusters
	int totph=0; // total number processed clusters


#ifndef FF

	Field<100, 0,1, double> eta_x;
	Field<100, 0,1, double> eta_y;
	if(etamap_x_filename.length() > 0 ){ 
		std::cout << "Trying to read eta map " << etamap_x_filename << std::endl;
		eta_x.read(etamap_x_filename);
	}
	if(etamap_y_filename.length() > 0 ){ 
		std::cout << "Trying to read eta map " << etamap_y_filename << std::endl;
		eta_y.read(etamap_y_filename);
	}

	Chart2d<100> chart(std::move(eta_x), std::move(eta_y));

	//eta2InterpolationPosXY interp (nsubpix, ff);
	//interp.prepareInterpolation();
	InterpolatedImage<int> totimg (NC, NR, nsubpix);

#else
	Field<> ff;
	cout << "Will write eta file " << etamap_filename << endl;
#endif

	BaseImage<double> gainmap(NC, NR);

	if (gainmap_filename.length() > 0 ) {
		std::cout << "Trying to read gain map " << gainmap_filename << std::endl;
		gainmap.read(gainmap_filename);
	} else {
		std::cout << "Using homogenous (reciprocal) gain: " << default_gain << std::endl;
		gainmap.get() = default_gain;
	}

	std::ifstream clustfile;
	clustfile.open(input_filename, std::ifstream::binary);

	if (! clustfile.is_open()) {
		cout << "could not open file " << input_filename << endl;

		return EXIT_FAILURE;
	}

	// file size
	clustfile.seekg(0, std::ifstream::end);
	size_t filesize = clustfile.tellg();
	clustfile.seekg(0);

	if ( 0 < filesize % clustersize ) {
		cout << "WARNING: Filesize " << filesize << " not multiple of cluster size " << clustersize << std::endl;
	}

	int num_chunks = filesize / buffer_bytes + 1;
	int processed_chunks = 0;


#pragma omp parallel
	{

		std::vector<single_photon_hit<3,3>> inbuf(buffersize);
#ifndef FF
		InterpolatedImage<int> intimg (NC, NR, nsubpix);
#else
		Field<> ff_private;
#endif

#pragma omp for
		for (int chunk = 0; chunk < num_chunks; chunk++) {

			size_t size;

#pragma omp critical
			{
				clustfile.read(reinterpret_cast<char*>(inbuf.data()), buffer_bytes);
				size = clustfile.gcount();
			}

			size_t numclusters = size / sizeof(single_photon_hit<3,3>);

			for (size_t i=0; i<numclusters; i++) {
				const single_photon_hit<3,3>& cl = inbuf[i];

#pragma omp atomic
				totph++;

				// drop clusters on the frame boundary
				if (! (cl.x > 0 && cl.x < NC-1 && cl.y > 0 && cl.y < NR-1))
					continue;

				photon_event event (cl, gainmap.get_const());

				// drop double events
				if (! (event.totquad/event.sum>0.8 && event.totquad/event.sum<1.2) )
					continue;

				// drop events out of energy range
				if (! (event.totquad>=cmin && event.totquad<cmax) )
					continue;

				// TODO: mask bad pixels

				std::array<double, 2> eta = event.eta;
				
                                /* eta can exceed the interval [0,1) due to negative charge-values
				* here, we wrap eta back into the unit interval and add the integer
			        * offset to the pixel coordinate */ 
#ifndef FF
				auto eta_offset = wrap(eta);
#else
				wrap(eta);
#endif

#pragma omp atomic
				++nph;

#ifndef FF
				if (do_interpolate){
				std::array<int,2> & quad_position = event.position;
//				std::array<double,2> intxy = interp.getInterpolatedPosition(eta[0], eta[1]);
				std::array<double,2> intxy = chart(eta);
				std::array<double,2> interpolated_position { quad_position[0] + eta_offset[0] + intxy[0], quad_position[1] + eta_offset[1] + intxy[1] };
				intimg[interpolated_position]++;
				}else{
					intimg(cl.x, cl.y)++;
				}
#else
				ff_private[eta]++;
#endif
			} // end cluster for loop

#pragma omp critical
			{
				++processed_chunks;
				std::cout << "processed chunk " << processed_chunks << "/" << num_chunks << std::endl;  
			}

		} // end chunk for loop


#pragma omp critical
		{
#ifndef FF
			// add to total image
			totimg.get() += intimg.get_const();
#else
			ff.get() += ff_private.get_const();
#endif

		}
	} // end parallel


#ifndef FF
	totimg.write(output_filename);
#else
	ff.write(etamap_filename);
#endif

	cout << "Filled " << nph << "/"<< totph <<" (" << (static_cast<double>(nph) / static_cast<double>(totph) * 100.) << "%)" << endl; 
	return 0;
}

