#include <iostream>
#include <fstream>

#include "cxxopts.hpp"

#include "single_photon_hit.h"
#include "histogram.h"
#include "interpolation.h"

using namespace std;
const int NC = 400; // number of columns
const int NR = 400; // number of rows

constexpr float default_gain = 1. / 140.;

constexpr size_t buffersize = 1024 * 1024;
constexpr size_t clustersize = sizeof(single_photon_hit<3,3>);
constexpr size_t buffer_bytes = clustersize * buffersize;

std::array<int,2> wrap(std::array<double,2> & input) {
  std::array<int,2> offset;

  offset[0] = std::floor(input[0]);
  offset[1] = std::floor(input[1]);

  input[0] -= offset[0];
  input[1] -= offset[1];

  return offset;
}


int main(int argc, char *argv[]) {

  cxxopts::Options options (argv[0], " - histogram of MÖNCH03 image");
  //	options.show_positional_help();

  options.add_options()
    ("g,gainmap", "Path to the gainmap", cxxopts::value<std::string>())
    ("m,cmin", "Minimum energy in keV", cxxopts::value<float>()->default_value("0"))
    ("M,cmax", "Maximum energy in keV", cxxopts::value<float>()->default_value("100"))
    ("n,bins", "Number of bins", cxxopts::value<unsigned>()->default_value("100"))
    ("h,help", "Print help")
    ("i,input", "Input file", cxxopts::value<std::vector<std::string>>());

  options.positional_help("<input> [<input2> ... ]");
  options.parse_positional({"input"});

  std::vector<std::string> input_file_list;
  std::string input_filename;
  std::string badmap_filename;
  std::string gainmap_filename;

  float cmin; // lower energy threshold in keV
  float cmax; // upper energy threshold in keV
  unsigned nbins;


  try {
    auto result = options.parse(argc, argv);

    if (result.count("help")) {
      std::cerr << options.help() << std::endl;
      return EXIT_SUCCESS;
    }


    if(result.count("gainmap")) {
      gainmap_filename = result["gainmap"].as<std::string>();
    }
    std::cerr << "gainmap: " << gainmap_filename << std::endl;

    cmin = result["cmin"].as<float>();
    std::cerr << "cmin: " << cmin << std::endl;

    cmax = result["cmax"].as<float>();
    std::cerr << "cmax: " << cmax << std::endl;

    nbins = result["bins"].as<unsigned>();
    std::cerr << "cmax: " << cmax << std::endl;

    if(result.count("input")) {
      auto &v = result["input"].as<std::vector<std::string>>();
      input_file_list = v;
    }


  } catch (const cxxopts::OptionException& e) {
    std::cerr << "error parsing options: " << e.what() << std::endl;
    return EXIT_FAILURE;
  }

  if (input_file_list.size() < 1) {
    std::cerr << "No input files given." << std::endl;
    return EXIT_FAILURE;
  }

  input_filename = input_file_list[0];

  std::cerr << "reading data from " << input_filename << std::endl;

  int nph=0;   // number of used clusters
  int totph=0; // total number processed clusters

  BaseImage<double> gainmap(NC, NR);

  if (gainmap_filename.length() > 0 ) {
    std::cerr << "Trying to read gain map " << gainmap_filename << std::endl;
    gainmap.read(gainmap_filename);
  } else {
    std::cerr << "Using homogenous (reciprocal) gain: " << default_gain << std::endl;
    gainmap.get() = default_gain;
  }

  std::ifstream clustfile;
  clustfile.open(input_filename, std::ifstream::binary);

  if (! clustfile.is_open()) {
    cerr << "could not open file " << input_filename << endl;

    return EXIT_FAILURE;
  }

  // file size
  clustfile.seekg(0, std::ifstream::end);
  size_t filesize = clustfile.tellg();
  clustfile.seekg(0);

  if ( 0 < filesize % clustersize ) {
    cerr << "WARNING: Filesize " << filesize << " not multiple of cluster size " << clustersize << std::endl;
  }

  int num_chunks = std::ceil(static_cast<double>(filesize) / buffer_bytes);
  int processed_chunks = 0;

  Histogram1d<float, int> hist(nbins, cmin, cmax);


#pragma omp parallel
  {

    std::vector<single_photon_hit<3,3>> inbuf(buffersize);

    Histogram1d<float, int> hist_p(nbins, cmin, cmax);

#pragma omp for
    for (int chunk = 0; chunk < num_chunks; chunk++) {

      size_t size;

#pragma omp critical
      {
        clustfile.read(reinterpret_cast<char*>(inbuf.data()), buffer_bytes);
        size = clustfile.gcount();
      }

      size_t numclusters = size / sizeof(single_photon_hit<3,3>);

      for (size_t i=0; i<numclusters; i++) {
        const single_photon_hit<3,3>& cl = inbuf[i];

#pragma omp atomic
        totph++;

        // drop clusters on the frame boundary
        if (! (cl.x > 0 && cl.x < NC-1 && cl.y > 0 && cl.y < NR-1))
          continue;

        photon_event event (cl, gainmap.get_const());

        // drop double events
        if (! (event.sum>cmin && event.totquad/event.sum>0.8 && event.totquad/event.sum<1.2 && event.sum<cmax) )
          continue;

	// energy mask
	if (event.totquad < cmin || event.totquad >= cmax)
		continue;

        // TODO: mask bad pixels

        double energy = event.totquad;
        hist_p.at(energy)++;

#pragma omp atomic
        ++nph;

      } // end cluster for loop

#pragma omp critical
      {
        ++processed_chunks;
        std::cerr << "processed chunk " << processed_chunks << "/" << num_chunks << std::endl;  
      }

    } // end chunk for loop


#pragma omp critical
    {
      // add to total image
      hist.data += hist_p.data;
    }
  } // end parallel

  hist.format(std::cout);

  cerr << "Filled " << nph << "/"<< totph <<" (" << (static_cast<double>(nph) / static_cast<double>(totph) * 100.) << "%)" << endl; 
  return 0;
}

