#!/bin/env python3

import numpy as np
from numpy import fft


def calc_a_x(mu_0):
    rhs = np.sum(mu_0, axis=1)
    C = mu_0.shape[1]
    a_x = rhs / C

    return a_x


def calc_b_y(mu_0, a_x):
    C = 1.
    b_y = mu_0 / (a_x * C)

    return b_y


def calc_a_and_b(mu_0):
    shape = mu_0.shape
    a_x_1d = calc_a_x(mu_0)
    a_x = np.repeat(a_x_1d.reshape(-1,1), shape[1], 1)
    b_y = calc_b_y(mu_0, a_x)

    a = np.cumsum(a_x, axis=0)
    b = np.cumsum(b_y, axis=1)

    return (a,b,)



# iterativer schmuuh


def get_fourier_coordinates(shape):
    k1 = fft.fftfreq(shape[0])
    k2 = fft.fftfreq(shape[1])
    K1 = np.repeat(k1.reshape(-1, 1), shape[1], axis=1)
    K2 = np.repeat(k2.reshape(1, -1), shape[0], axis=0)

    return (K1, K2, )


def get_identity(shape):
    x1 = np.linspace(0, shape[0], shape[0])
    x2 = np.linspace(0, shape[0], shape[1])

    X1 = np.repeat(x1.reshape(-1, 1), shape[1], axis=1)
    X2 = np.repeat(x2.reshape(1, -1), shape[0], axis=0)

    return (X1, X2, )


def invert_laplace(g):
    # invert "\laplace f = g, f = 0 on the boundary"
    fourier_g = fft.fft2(g)

    # construct fourier kernel
    k1, k2 = get_fourier_coordinates(g.shape)
    k_squared = k1 ** 2 + k2 ** 2
    fourier_kernel = np.zeros(g.shape)
    fourier_kernel[k_squared > 0] = - 1 / (2 * np.pi) ** 2 / k_squared[k_squared > 0]

    fourier_f = fourier_g * fourier_kernel

    f = np.real(fft.ifft2(fourier_f))
    return f


def dx(f):
    dxf = np.gradient(f, axis=0)
    dxf[0,:] = (f[1,:] - f[-1,:]) / 2
    dxf[-1,:] = (f[0,:] - f[-2,:]) / 2
    return dxf


def dy(f):
    dyf = np.gradient(f, axis=1)
    dyf[:,0] = (f[:,1] - f[:,-1]) / 2
    dyf[:,-1] = (f[:,0] - f[:,-2]) / 2
    return dyf


def rot90(u):
    return -u[1], u[0]


def div(u):
    return dx(u[0]) + dy(u[1])


def curl(u):
    return dx(u[1]) - dy(u[0])


def project_divergence_free(u):
    u_perp = rot90(u)
    g = -div(u_perp)

    f = invert_laplace(g)
    grad_f = (dx(f), dy(f))

    return rot90(grad_f)


def calc_jacobian(u):
    return [[dx(u[0]), dy(u[0])],
            [dx(u[1]), dy(u[1])]]


def det(A):
    return A[0][0] * A[1][1] - A[0][1] * A[1][0]


def calc_u_t(u_0, mu_0):
    chi = project_divergence_free(u_0)
    mu_0_inv = 1 / mu_0
    du = calc_jacobian(u_0)

    u_t_1 = - mu_0_inv * ((du[0][0]+1) * chi[0] + du[0][1] * chi[1])
    u_t_2 = - mu_0_inv * (du[1][0] * chi[0] + (du[1][1]+1) * chi[1])

    return u_t_1, u_t_2


def calc_monge_kantorovich(delta, mu_0):
    s = mu_0.shape

    m_mk = np.sum((delta[0]) ** 2 + (delta[1]) ** 2 * mu_0) / (s[0] * s[1])
    return m_mk


def calc_u_gradient_descent(u_0, mu_0, max_iterations=1000, gamma=1e-2, epsilon=1e-2):
    x = get_identity(mu_0.shape)
    u = (u_0[0] - x[0], u_0[1] - x[1])

    m_mk = np.zeros(max_iterations + 1)
    m_mk[0] = calc_monge_kantorovich(u, mu_0)

    iteration = 0

    print(f"{m_mk[0]:.2e}", end='')

    for iteration in range(1, max_iterations + 1):
        u_t = calc_u_t(u, mu_0)

        u_neu = (u[0] + gamma * u_t[0], u[1] + gamma * u_t[1])
        m_mk[iteration] = calc_monge_kantorovich(u_neu, mu_0)

        print(f"\r{m_mk[iteration]:.2e}", end='')

        if m_mk[iteration] > m_mk[iteration - 1]:
            iteration = iteration - 1
            break

        u = u_neu

        if m_mk[iteration - 1] - m_mk[iteration] < epsilon * gamma:
            break

    u_abs = (u[0] + x[0], u[1] + x[1])

    return u_abs, m_mk, iteration


# convenience functions

def rebin(a, shape):
    sh = shape[0], a.shape[0] // shape[0], shape[1], a.shape[1] // shape[1]
    return a.reshape(sh).mean(-1).mean(1)


if __name__ == '__main__':

    import argparse
    import imageio

    parser = argparse.ArgumentParser(prog='etamap')
    parser.add_argument("input")
    parser.add_argument("--output_x", "-x", nargs='?')
    parser.add_argument("--output_y", "-y", nargs='?')
    parser.add_argument("--iterations", "-i", nargs='?', type=int, help="Maximum number of iterations", default=1000)
    parser.add_argument("--step", "-s", nargs='?', type=float, help='Step size', default=1e-2)
    parser.add_argument("--numstart", "-n", nargs='?', type=int, help='start of input file numbers - if used input is file prefix')
    parser.add_argument("--numend",  "-N", nargs='?', type=int, help='end of input file numbers - if used input is file prefix')

    args = parser.parse_args()
    
    if args.numstart is None:
        print(f"Reading file {args.input}")
        etamap = imageio.imread(args.input)
    else:
        if args.numend is None:
            print('numstart is given but numend is not defined ... abort.')
        Ni = args.numend-args.numstart+1
        fname=f'{args.input}_{args.numstart}.tiff'
        print(f"Reading file {fname}")
        etamap = imageio.imread(fname)

        for i in range(1,Ni):
            fname=f'{args.input}_{args.numstart+i}.tiff'
            print(f"Reading file {fname}")
            etamap += imageio.imread(fname)
            

    eta = rebin(np.array(etamap).T, (100,100,))

    # normalize
    mu_0 = eta * eta.size / np.sum(eta.ravel())
    u_0 = calc_a_and_b(mu_0)

    print(f"Running gradient descent with max_iterations={args.iterations} and step_size={args.step}")
    u, M, i_last = calc_u_gradient_descent(u_0, mu_0, max_iterations=args.iterations, gamma=args.step)

    print(f"\nGradient descent stopped after {i_last} iterations")

    # compare initial and final curl
    curl0_f = curl(u_0)
    curl1_f = curl(u)

    curl0 = np.sqrt((curl0_f**2).sum())
    curl1 = np.sqrt((curl1_f**2).sum())

    print("Initial solution:")
    print(f"  Monge Kantorovich: {M[0]:.2e}, total L2-curl: {curl0:.2e}")
    print("Final solution:")
    print(f"  Monge Kantorovich: {M[i_last]:.2e}, total L2-curl: {curl1:.2e}")

    # remove values exceeding 1
    for i in range(2):
        u[i][u[i] > mu_0.shape[i]] = mu_0.shape[i]
        u[i][u[i] < 0] = 0

    u_norm_0 = (u[0] / mu_0.shape[0]).astype(np.float32)
    u_norm_1 = (u[1] / mu_0.shape[1]).astype(np.float32)

    print(f"Saving solution into {args.output_x} and {args.output_y}.")
    imageio.imwrite(args.output_x, u_norm_0.T)
    imageio.imwrite(args.output_y, u_norm_1.T)
