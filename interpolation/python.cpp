#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>

#include "moench.h"

namespace py = pybind11;

PYBIND11_MODULE(_moench, m) {
	m.doc() = "MOENCH detector io";
	

	m.def("compute_hitmaps", &moench::compute_hitmaps, py::return_value_policy::move);
  m.def("compute_image", &moench::compute_image, py::return_value_policy::move);

  
	m.attr("dx") = py::int_(moench::dx); 
	m.attr("dy") = py::int_(moench::dy);
	//m.attr("hitmap_resolution") = py::int_(moench::hitmap_resolution);

}	
