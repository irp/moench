import numpy as np

from . import _moench


class ClusterFileProcessor(_moench.ClusterFileProcessor):
    def __init__(self, gainmap):
        gainmap_t = gainmap.transpose().astype(np.float32)
        super().__init__(gainmap_t)

    def process_file(self, filename, min_energy, max_energy, n_bins):
        images_raw = super().process_file_raw(filename, min_energy, max_energy, n_bins)
        n_int = 1
        images = images_raw.reshape([n_bins, _moench.ny * n_int, _moench.ny * n_int])

        return images
