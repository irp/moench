import numpy as np
from scipy.fft import fftfreq, fft2, ifft2
import numpy.linalg as la


# some basic 2d vector analysis analysis algebra


def calc_jacobian(u):
    grad_u = np.gradient(u, axis=(0, 1))

    # optional: compute boundary values separately

    jac = np.stack(grad_u, axis=-1)

    return jac


def div(u):
    jac = calc_jacobian(u)
    return jac[..., 0, 0] + jac[..., 1, 1]


def curl(u):
    jac = calc_jacobian(u)
    return jac[..., 1, 0] - jac[..., 0, 1]


def rot90(u):
    return np.flip(u, axis=-1) * np.asarray([-1, 1])


_xaxis = 0
_yaxis = 1


def calc_a_x(mu_0):
    return np.sum(mu_0, axis=_yaxis) / mu_0.shape[_yaxis]


def calc_b_y(mu_0, a_x):
    return mu_0 / a_x


def calc_u_0(mu_0):
    shape_u_0 = mu_0.shape + (2,)
    u_0 = np.zeros_like(mu_0, shape=shape_u_0)

    a_x = calc_a_x(mu_0).reshape(-1, 1)
    b_y = calc_b_y(mu_0, a_x)

    u_0[..., 0] = np.cumsum(a_x, axis=_xaxis)
    u_0[..., 1] = np.cumsum(b_y, axis=_yaxis)

    return u_0


def get_identity(shape):
    shape_id = shape + (2,)
    iden = np.zeros(shape=shape_id)

    iden[..., 0] = np.arange(shape[0]).reshape(-1, 1)
    iden[..., 1] = np.arange(shape[1]).reshape(1, -1)

    return iden


def invert_laplace(g):
    # invert "\laplace f = g, f = 0 on the boundary"

    # construct fourier kernel
    k1 = fftfreq(g.shape[0]).reshape(-1, 1)
    k2 = fftfreq(g.shape[1]).reshape(1, -1)
    k_squared = k1**2 + k2**2
    mask = k_squared > 0
    fourier_kernel = -1 / (2 * np.pi) ** 2 / (k_squared + ~mask) * mask

    fourier_g = fft2(g)
    fourier_f = fourier_g * fourier_kernel
    f = np.real(ifft2(fourier_f))

    return f


def project_divergence_free(u):
    u_perp = rot90(u)
    g = -div(u_perp)

    f = invert_laplace(g)
    grad_f = calc_jacobian(f)

    return rot90(grad_f)


def calc_u_t(u_0, mu_0):
    chi = project_divergence_free(u_0)
    du = calc_jacobian(u_0)
    u_t = -1 / mu_0[..., np.newaxis] * ((du + np.eye(2)) @ chi[..., np.newaxis]).squeeze()

    return u_t


def calc_monge_kantorovich(delta, mu_0):
    m_mk = np.sum(delta**2 * mu_0[..., np.newaxis]) / np.prod(mu_0.shape)

    return m_mk


def calc_u_gradient_descent(u_0, mu_0, max_iterations=1000, gamma=1e-2, epsilon=1e-3):
    x = get_identity(mu_0.shape)
    delta_u = u_0 - x

    m_mk = np.zeros(max_iterations + 1)
    m_mk[0] = calc_monge_kantorovich(delta_u, mu_0)

    iteration = 0

    print(f"{m_mk[iteration]:.2e}", end="")

    for iteration in range(1, max_iterations + 1):
        u_t = calc_u_t(delta_u, mu_0)
        delta_u_neu = delta_u + gamma * u_t
        m_mk[iteration] = calc_monge_kantorovich(delta_u_neu, mu_0)

        print(f"\r{m_mk[iteration]:.2e}", end="")

        if m_mk[iteration] > m_mk[iteration - 1]:
            iteration = iteration - 1
            print(f"\nMonge-Kantorovich metric increased in iteration {iteration}")
            break

        delta_u = delta_u_neu

        if m_mk[iteration - 1] - m_mk[iteration] < epsilon * gamma:
            print(f"\nConverged in {iteration}")
            break

    u = delta_u + x

    return u, m_mk, iteration


def compute_psi(eta, max_iterations=1000, step=1e-2, epsilon=1e-3):
    # normalize
    mu_0 = eta * eta.size / np.sum(eta.ravel())

    # compute initial guess
    u_0 = calc_u_0(mu_0)

    print(f"Running gradient descent with max_iterations={max_iterations} and step_size={step}")
    u, m_mk, i_last = calc_u_gradient_descent(
        u_0, mu_0, max_iterations=max_iterations, gamma=step, epsilon=epsilon
    )

    print(f"\nGradient descent stopped after {i_last} iterations")

    # compare initial and final curl
    curl0_f = curl(u_0)
    curl1_f = curl(u)

    curl0 = la.norm(curl0_f.ravel())
    curl1 = la.norm(curl1_f.ravel())

    print("Initial solution:")
    print(f"  Monge Kantorovich: {m_mk[0]:.2e}, total L2-curl: {curl0:.2e}")
    print("Final solution:")
    print(f"  Monge Kantorovich: {m_mk[i_last]:.2e}, total L2-curl: {curl1:.2e}")

    # normalize
    u_norm = (u / np.asarray(mu_0.shape)).astype(np.float32)

    # clip
    u_norm[u_norm > 1] = 1
    u_norm[u_norm < 0] = 0

    return u_norm[..., 0].T.copy(), u_norm[..., 1].T.copy(), m_mk
